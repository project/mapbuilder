<?php

/**
 * dom_DOMDocument for PHP4
 */
class dom_DOMDocument {
  
  var $doc;
  
  function dom_DOMDocument($version = '1.0', $encoding = 'UTF-8') {
    $this->doc = domxml_new_doc($version);
  }
  
  function loadXML($xml) {
    $this->doc = domxml_open_mem($xml);
  }
  
  function saveXML() {
    return $this->doc->dump_mem(true);
  }
  
  function createElement($tagName, $value = NULL) {
    $node = $this->doc->create_element($tagName);
    if ($value) {
      $node->append_child($this->doc->create_text_node($value));
    }
    return $node;
  }
  
  function createTextNode($text) {
    return $this->doc->create_text_node($text);
  }
  
  function createComment($text) {
    return $this->doc->create_comment($text);
  }
  
  function getElementsByTagName($tagName) {
    return $this->doc->get_elements_by_tagname($tagName);
  }

  function getNamespace() {
    $elem = $this->doc->document_element();
    if ($elem->tagname() == 'MapbuilderConfig')
      return "http://mapbuilder.sourceforge.net/mapbuilder";
    else if ($elem->tagname() == 'ViewContext')
      return "http://www.opengis.net/context";
    echo 'Missing namespace for document_element:'.var_export($elem,true).'<br/>';
    //return $elem->namespace;
  }

}

/**
 * dom_DOMXPath for PHP4
 */
class dom_DOMXPath {
  
  var $xp;
  
  function dom_DOMXPath($domdoc) {
    $this->xp = xpath_new_context($domdoc->doc);
  }
  
  function registerNamespace($prefix, $namespace) {
    xpath_register_ns($this->xp, $prefix, $namespace);
  }
  
  function query($query, $domnode = NULL) {
    if ($domnode)
      $xpresult = xpath_eval($this->xp, $query, $domnode);
    else
      $xpresult = xpath_eval($this->xp, $query);
    
    return $xpresult->nodeset;
  }
  
}

function dom_getAttribute($elem, $name) {
  return $elem->get_attribute($name);
}

function dom_hasAttribute($elem, $name) {
  return $elem->has_attribute($name);
}

function dom_setAttribute($elem, $name, $value) {
  return $elem->set_attribute($name, $value);
}

function dom_appendChild($parent, $child) {
  return $parent->append_child($child);
}

function dom_getElementsByTagName($elem, $tagName) {
  return $elem->get_elements_by_tagname($tagName);
}

function dom_getListItem($list, $index) {
  return $list[$index];
}

function dom_getListSize($list) {
  return count($list);
}

function dom_getTagName($elem) {
  return $elem->tagname();
}

function dom_getValue($elem) {
  if ($elem->type == XML_ELEMENT_NODE) {
    $child = $elem->first_child();
    return $child->node_value();
  }
  return $elem->node_value();
}

/**
 * array_combine
 */
function array_combine( $keys, $vals ) {
  $keys = array_values( (array) $keys );
  $vals = array_values( (array) $vals );
  $n = max( count( $keys ), count( $vals ) );
  $r = array();
  for( $i = 0; $i < $n; $i++ ) {
    $r[ $keys[ $i ] ] = $vals[ $i ];
  }
  return $r;
}

?>
