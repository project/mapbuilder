REQUIREMENTS

This module requires:
- the community mapbuilder library; I suggest the stable release of mapbuilder. 
  Use the zip download, or alternatively the src download if you want to read 
  or make changes to the mapbuilder sources.
- PHP 5 with support for DOM functions; when building PHP yourself use the
  --with-xml and --with-curl configuration options
- the curl extension for php (e.g. package php5-curl under debian linux)

REMARKS

This code is tested with Firefox 1.5 under Debian/GNU Linux. No modifications
to mapbuilder sources were needed.

Steps to test:

1) in the mapbuilder folder extract the community mapbuilder sources 
   e.g. in mapbuilder-src

2) enable the mapbuilder module in admin/modules

3) check the location of the mapbuilder library in admin/settings/mapbuilder

4) add an input format in admin/filters for mapbuilder and choose the filters
   'Expand mapbuilder tags' (weight 2) and 'HTML filter' (weight 1), and
   optionally the 'Line break converter'.
   Configure the filter and add the '<map>' tag to the Allowed HTML tags.

5) edit your access control for mapbuilder in admin/user/access.

6) try to make a new page using the mapbuilder filter and content e.g.

   <map>mapbuilder/config/1</map>
   
   In this example, '1' represents the drupal node id of the mapconfig node. 
   Replace this value by the id of your own node.
   
   or
   
   <map config="path/to/config" />
   
   The config path is relative to the drupal base_path.
   E.g.
   
   <map config="sites/all/modules/mapbuilder/demo/config.xml" />
   
   
   If a mapbuilder context node has been added, it can be used in a config
   node using the relative path 'mapbuilder/context/<node id>'.
   A mapbuilder config node can be used as 'mapbuilder/config/<node id>'.

   To try a file, e.g. <map>sites/all/modules/mapbuilder/demo/config.xml</map>.
   Note that this file assumes that the base_path is '/drupal-5.0'. If it is 
   not, replace it by your own value.
   
   The <defaultModelUrl> and <skinDir> paths in a config file contain 
   the base_path because these are processed by Mapbuilder. 

