<?php

/**
 * dom_DOMDocument for PHP5
 */
class dom_DOMDocument {
  
  var $doc;
  
  function dom_DOMDocument($version = '1.0', $encoding = 'UTF-8') {
    $this->doc = new DOMDocument($version, $encoding);
  }
  
  function loadXML($xml) {
    $this->doc->loadXML($xml);
  }
  
  function saveXML() {
    $this->doc->formatOutput = TRUE;
    return $this->doc->saveXML();
  }
  
  function createElement($tagName, $value = NULL) {
    return $this->doc->createElement($tagName, $value);
  }
  
  function createTextNode($text) {
    return $this->doc->createTextNode($text);
  }
  
  function createComment($text) {
    return $this->doc->createComment($text);
  }
  
  function getElementsByTagName($tagName) {
    return $this->doc->getElementsByTagName($tagName);
  }

  function getNamespace() {
//echo $this->doc->documentElement->namespaceURI . '<br/>';
    return $this->doc->documentElement->namespaceURI;
  }

}

/**
 * dom_DOMXPath for PHP5
 */
class dom_DOMXPath {
  
  var $xp;
  
  function dom_DOMXPath($domdoc) {
    $this->xp = new DOMXPath($domdoc->doc);
  }
  
  function registerNamespace($prefix, $namespace) {
    $this->xp->registerNamespace($prefix, $namespace);
  }
  
  function query($query, $domnode = NULL) {
    if ($domnode)
      return $this->xp->query($query, $domnode);
    else
      return $this->xp->query($query);
  }
  
}

function dom_getAttribute($elem, $name) {
  return $elem->getAttribute($name);
}

function dom_hasAttribute($elem, $name) {
  return $elem->hasAttribute($name);
}

function dom_setAttribute($elem, $name, $value) {
  return $elem->setAttribute($name, $value);
}

function dom_appendChild($parent, $child) {
  //echo var_export(array($parent,$child),true) . '<br/>';
  return $parent->appendChild($child);
}

function dom_getElementsByTagName($elem, $tagName) {
  return $elem->getElementsByTagname($tagName);
}

function dom_getListItem($list, $index) {
  return $list->item($index);
}

function dom_getListSize($list) {
  return $list->length;
}

function dom_getTagName($elem) {
  return $elem->tagName;
}

function dom_getValue($elem) {
  return $elem->nodeValue;
}

?>
