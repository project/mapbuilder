/*
License: LGPL as per: http://www.gnu.org/copyleft/lesser.html
Dependancies: Context
*/

// Ensure this object's dependancies are loaded.
mapbuilder.loadScript(baseDir+"/tool/ButtonBase.js");
/**
 * Builds then sends a WFS GetFeature GET request based on the WMC
 * coordinates and click point.
 * @constructor
 * @base ButtonBase
 * @author Cameron Shorter
 * @param widgetNode The XML node in the Config file referencing this object.
 * @param model The Context object which this tool is associated with.
 */
function WfsGetFeature(widgetNode, model) {
  // Extend ButtonBase
  ButtonBase.apply(this, new Array(widgetNode, model));
  this.tolerance= widgetNode.selectSingleNode('mb:tolerance').firstChild.nodeValue;
  
  var node = widgetNode.selectSingleNode('mb:typeName');
  if (node != null) {
    this.typeName = node.firstChild.nodeValue;
  }
  
  var node = widgetNode.selectSingleNode('mb:maxFeatures');
  if (node != null) {
    this.maxFeatures = node.firstChild.nodeValue;
  }
  
  node = widgetNode.selectSingleNode('mb:webServiceUrl');
  if (node != null) {
    this.webServiceUrl= node.firstChild.nodeValue;
  }
  
  this.httpPayload=new Object();
  this.httpPayload.method="get";
  this.httpPayload.postData=null;
  
  this.trm=widgetNode.selectSingleNode("mb:transactionResponseModel").firstChild.nodeValue;

  // override default cursor by user
  // cursor can be changed by spefying a new cursor in config file
  this.cursor = "pointer"; 

  /**
   * Open window with query info.
   * This function is called when user clicks map with Query tool.
   * @param objRef      Pointer to this GetFeatureInfo object.
   * @param targetNode  The node for the enclosing HTML tag for this widget.
   */
  this.doAction = function(objRef,targetNode) {
    if (objRef.enabled) {
      extent=objRef.targetModel.extent;
      point=extent.getXY(targetNode.evpl);
      xPixel=extent.res[0]*objRef.tolerance;
      yPixel=extent.res[1]*objRef.tolerance;
      bbox=(point[0]-xPixel)+","+(point[1]-yPixel)+","+(point[0]+xPixel)+","+(point[1]+yPixel);
      
      var wfsUrl = objRef.webServiceUrl;
      
      var selectedLayer = objRef.targetModel.getParam('selectedLayer');
      var typeName;
      if (selectedLayer) {
        // query selected layer
        var layer = objRef.targetModel.getLayer(selectedLayer);
        // derive wfsUrl
        wfsUrl = objRef.targetModel.getServerUrl('GetMap', 'GET', layer);
        var ch = wfsUrl.indexOf('?') < 0 ? '?' : '&';
        wfsUrl += ch + 'version=1.0.0&service=WFS';
        typeName = selectedLayer;
      }
      else {
        // try typeName from config or all queryable layers
        typeName = objRef.typeName;
        if (!typeName) {
          var queryList=objRef.targetModel.getQueryableLayers();
          if (queryList.length==0) {
            alert('There are no queryable layers available,\nplease add a queryable layer to the context.');
            return;
          }
          else {
            typeName = "";
            for (var i=0; i<queryList.length; ++i) {
              var layerNode=queryList[i];
              var layerName=layerNode.firstChild.data;
              var hidden = objRef.targetModel.getHidden(layerName);
              if (hidden == 0) {
                //query only visible layers
                if (typeName != "") {
                  typeName += ",";
                }
                else if (!wfsUrl) {
                  // first layer; guess a wfsUrl
                  var layer = objRef.targetModel.getLayer(layerName);
                  // derive wfsUrl
                  wfsUrl = objRef.targetModel.getServerUrl('GetMap', 'GET', layer);
                  var ch = wfsUrl.indexOf('?') < 0 ? '?' : '&';
                  wfsUrl += ch + 'version=1.0.0&service=WFS';
                }
                typeName += layerName;
              }
            }
          }
        }
  
        if (typeName=="") {
          alert('There are no queryable layers visible,\nplease mark a queryable layer in the legend.');
          return;
        }
      }
      
      // get bbox from aoi
      var aoi = objRef.targetModel.getParam('aoi');
      var ext = new Array();
      ext[0] = aoi[0][0];
      ext[1] = aoi[0][1];
      ext[2] = aoi[1][0];
      ext[3] = aoi[1][1];
      if (ext[2] < ext[0]) {
        var tmp = ext[2];
        ext[2] = ext[0];
        ext[0] = tmp;
      }
      if (ext[3] < ext[1]) {
        var tmp = ext[3];
        ext[3] = ext[1];
        ext[1] = tmp;
      }
      // todo: add tolerance?
      bbox = ext.join(',');
      
      objRef.httpPayload.url = wfsUrl + (wfsUrl.indexOf('?') < 0 ? '?' : '&')
        + "request=GetFeature&typeName=" + typeName + "&bbox="+bbox;
      if (objRef.maxFeatures) {
        objRef.httpPayload.url += '&maxFeatures=' + objRef.maxFeatures;
      }
      
      // Model that will be populated with the WFS response.
      if (!objRef.transactionResponseModel) {
        objRef.transactionResponseModel = config.objects[objRef.trm];
        //objRef.transactionResponseModel.addListener("loadModel",objRef.handleResponse, objRef);
      }
      objRef.transactionResponseModel.newRequest(objRef.transactionResponseModel,objRef.httpPayload);
    }
  }
  

  /**
   * Register for mouseUp events.
   * @param objRef  Pointer to this object.
   */
  this.setMouseListener = function(objRef) {
    if (objRef.mouseHandler) {
      objRef.mouseHandler.model.addListener('mouseup',objRef.doAction,objRef);
    }
    objRef.context=objRef.widgetNode.selectSingleNode("mb:context");
    if (objRef.context){
      objRef.context=eval("config.objects."+objRef.context.firstChild.nodeValue);
    }
  }
  config.addListener( "loadModel", this.setMouseListener, this );
  
  /**
   * Clear current selection when this button is selected.
   * @param selected True when selected.
   * @param objRef  Pointer to this object.
   */
  this.doSelect = function(selected,objRef) {
    if (selected) {
      if (!objRef.transactionResponseModel) {
        objRef.transactionResponseModel = config.objects[objRef.trm];
      }
      // empty current selection
      objRef.transactionResponseModel.setModel(objRef.transactionResponseModel,null);
    }
  }
}
