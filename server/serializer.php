<?php

////////////////////////////////////////////////////////////////////////////////
// Description:
// Script to serialize incoming data and return a reference to the file.
//
// Author: Nedjo Rogers
////////////////////////////////////////////////////////////////////////////////


// Set save directory.  This should be set to a writable, web-accessible 
// directory, relative to the drupal installation

//$outputPath = $_GET['file_directory_temp'];
$base_path = $_GET['base_path'];
$file_directory_temp = $_GET['file_directory_temp'];

// remove base_path from this script_filename
$script_name = substr($_SERVER['SCRIPT_NAME'], strlen($base_path));
$temp_filepath = str_replace($script_name, $file_directory_temp, $_SERVER['SCRIPT_FILENAME']) . '/';
$temp_webpath = $base_path . $file_directory_temp . '/';

// $_SERVER['SCRIPT_FILENAME'] = install_dir + script_name

// Read in data.
$data = $GLOBALS["HTTP_RAW_POST_DATA"];

// Create temp file
$tmpfname = tempnam($temp_filepath, "cmb");

// save data to it
$handle = fopen($tmpfname, "w");
fwrite($handle, $data);
fclose($handle);
// rename to .xml
if (rename($tmpfname, $tmpfname.'.xml')) $tmpfname .= '.xml';

// Send xml content type header.
header("Content-type: text/xml");

echo '<!-- ' . var_export(array('_GET'=>$_GET,'temp_filepath'=>$temp_filepath,'temp_webpath'=>$temp_webpath,'tmpfname'=>$tmpfname,'file_directory_temp'=>$file_directory_temp),true) . ' -->';

// Return XML snippet with file reference
echo '
<XmlSerializer xmlns:xlink="http://www.w3.org/1999/xlink">
  <OnlineResource xlink:type="simple" xlink:href="' 
  . $temp_webpath . basename($tmpfname)
  . '"/>
</XmlSerializer>';

?>
